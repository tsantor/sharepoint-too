# History
All notable changes to this project will be documented in this file. This project adheres to [Semantic Versioning](http://semver.org/).

## 0.1.0 (2020-04-27)
* First release on PyPI.

## 0.1.1 (2020-04-27)
* Updated license and installaton instructions due to typo.

## 0.1.2 (2020-06-08)
* When uploading a file to SharePoint we use just the filename and not the full file path.


## 0.2.0 (2021-09-23)
* **Changed**: Now you must pass on open file as content along with desired filename. We can't assume the file is local, it may be local or in the cloud. Such as with Django storages.
